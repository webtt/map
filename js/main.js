document.addEventListener('DOMContentLoaded', function() {

    ;
    ymaps.ready(function () {
        const iconImageSize = [26, 26];
        const iconImageOffset = [-10, -30];
        const iconContentOffset = [15, 300];
        var myMap = new ymaps.Map('map', {
            center: [55.751574, 37.613856],
            zoom: 12,
            controls: []

        }, {
            searchControlProvider: 'yandex#search'
        });

        // let dataJsonConfig = jQuery.getJSON('data.json').json();
        let dataJsonConfig = [
            {
                "cords": [
                    "55.738632",
                    "37.608357"
                ],
                "hintContent": "Принцесса лягушка",
                "bgImage": "img/bg/popup.jpg",
                "iconImageHref": "img/map-icons/svg/015-frog prince.svg",
                "title": "Arka Bar",
                "desc": "Во все времена петербуржцы выпивали в Арке. Традициям города изменять не стоит.",
                "tag": "#arkabar",
                "address": "Санкт-Петербург, ул. Большая Конюшенная, 17",
                "contentBtn": "Push"
            },
            {
                "cords": [
                    "55.731779",
                    "37.650263"
                ],
                "hintContent": "Русалка",
                "bgImage": "img/bg/tips1.jpg",
                "iconImageHref": "img/map-icons/svg/030-mermaid.svg",
                "title": "РусАлkоBar",
                "desc": "2Во все времена петербуржцы выпивали тут. Традициям города изменять не стоит.",
                "tag": "#arkabar2",
                "address": "Санкт-Петербург, ул. Большая Конюшенная, 27",
                "contentBtn": "Pushh"
            },
            {
                "cords": [
                    "55.75957",
                    "37.614982"
                ],
                "hintContent": "Огр",
                "bgImage": "img/bg/tips2.jpg",
                "iconImageHref": "img/map-icons/svg/006-ogre.svg",
                "title": "OgreBar",
                "desc": "Во все времена петербуржцы выпивали тут. Традициям города изменять не стоит.",
                "tag": "#arkabar3",
                "address": "Санкт-Петербург, ул. Большая Конюшенная, 37",
                "contentBtn": "Pushhh"
            }
        ];

        dataJsonConfig.forEach((item) => {
            myMap.geoObjects.add(
                new ymaps.Placemark(item.cords, {
                    hintContent: item.hintContent,
                    balloonHeader: item.hintContent,
                    bgImage: item.bgImage,
                    iconImageHref: item.iconImageHref,
                    title: item.title,
                    desc: item.desc,
                    tag: item.tag,
                    address: item.address,
                    contentBtn: item.contentBtn
                }, {
                    // Опции.
                    // Необходимо указать данный тип макета.
                    iconLayout: 'default#imageWithContent',
                    // Своё изображение иконки метки.
                    iconImageHref: item.iconImageHref,
                    // Размеры метки.
                    iconImageSize: iconImageSize,
                    // Смещение левого верхнего угла иконки относительно
                    // её "ножки" (точки привязки).
                    iconImageOffset: iconImageOffset,
                    // Смещение слоя с содержимым относительно слоя с картинкой.
                    iconContentOffset: iconContentOffset
                })
            )
        });


        observeEvents(myMap);

        function observeEvents(map) {
            map.geoObjects.each(function (geoObject) {

                let placemarkPopup = document.querySelector('.pz-popup');
                let placemarkPopupOverlay = document.querySelector('.pz-popup__overlay');

                geoObject.events.add('click', function (e1) {
                    let placemark = e1.get('target');
                    placemark = placemark.properties._data

                    placemarkPopupOverlay.style.display = 'block';
                    placemarkPopup.classList.add('pz-popup_active');
                    placemarkPopup.style.backgroundImage = `url('${placemark.bgImage}')`;
                    placemarkPopup.querySelector('.pz-popup-content-info__icon img').src = placemark.iconImageHref;
                    placemarkPopup.querySelector('.pz-popup-content__title').innerText = placemark.title;
                    placemarkPopup.querySelector('.pz-popup-content__desc').innerText = placemark.desc;
                    placemarkPopup.querySelector('.pz-popup-content__tag').innerText = placemark.tag;
                    placemarkPopup.querySelector('.pz-popup-content-info__text-address').innerText = placemark.address;
                    placemarkPopup.querySelector('.pz-popup-content-btn').innerText = placemark.contentBtn;

                    let placemarkPopupClose = document.querySelector('.pz-popup__close');
                    placemarkPopupClose.addEventListener('click', function() {
                        placemarkPopup.classList.remove('pz-popup_active');
                        setTimeout(function() {
                            placemarkPopupOverlay.style.display = 'none';
                        }, 800);
                    })
                });

            });
        }

    });






















    document.querySelector('.pz-collections-head__how').addEventListener('click', function() {
        document.querySelector('.pz-collections-content__popup').classList.add('pz-section-content__popup_active');
    })
    document.querySelector('.pz-section-content__popup__close').addEventListener('click', function() {
        document.querySelector('.pz-collections-content__popup').classList.remove('pz-section-content__popup_active');
    })

    document.querySelector('.pz-weekends-head__how').addEventListener('click', function() {
        document.querySelector('.pz-weekends-content__popup').classList.add('pz-section-content__popup_active');
    })
    document.querySelector('.pz-weekends-content__popup__close').addEventListener('click', function() {
        document.querySelector('.pz-weekends-content__popup').classList.remove('pz-section-content__popup_active');
    })

    document.querySelector('.pz-discounts-head__how').addEventListener('click', function() {
        document.querySelector('.pz-discounts-content__popup').classList.add('pz-section-content__popup_active');
    })
    document.querySelector('.pz-discounts-content__popup__close').addEventListener('click', function() {
        document.querySelector('.pz-discounts-content__popup').classList.remove('pz-section-content__popup_active');
    })

    document.querySelector('.pz-places-head__how').addEventListener('click', function() {
        document.querySelector('.pz-places-content__popup').classList.add('pz-section-content__popup_active');
    })
    document.querySelector('.pz-places-content__popup__close').addEventListener('click', function() {
        document.querySelector('.pz-places-content__popup').classList.remove('pz-section-content__popup_active');
    })




    new WOW().init();

    var pzTipsContent = new Swiper(document.querySelector('.pz-tips-content'), {
        slidesPerView: 'auto',
        freeMode: true
    });
    var pzCollectionsContent = new Swiper(document.querySelector('.pz-collections-content'), {
        slidesPerView: 'auto',
        freeMode: true
    });
    var pzWeekendsContent = new Swiper(document.querySelector('.pz-weekends-content'), {
        slidesPerView: 'auto',
        freeMode: true
    });
    var pzDiscountsContent = new Swiper(document.querySelector('.pz-discounts-content'), {
        slidesPerView: 'auto',
        freeMode: true
    });
    var pzPlacesContent = new Swiper(document.querySelector('.pz-places-content'), {
        slidesPerView: 'auto',
        freeMode: true
    });




}, false);
